
#pragma once

#include <cstdint>

#include "OvTime.h"

// Ceiling function for fixed point 32:32 bit uints
ovtime_t ceilFixedPoint(ovtime_t value);

