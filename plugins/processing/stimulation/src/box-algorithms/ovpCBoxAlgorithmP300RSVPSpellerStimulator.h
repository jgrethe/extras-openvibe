#ifndef __OpenViBEPlugins_BoxAlgorithm_P300RSVPSpellerStimulator_H__
#define __OpenViBEPlugins_BoxAlgorithm_P300RSVPSpellerStimulator_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_P300RSVPSpellerStimulator     OpenViBE::CIdentifier(0x48E7E6C5, 0x23D540A0)
#define OVP_ClassId_BoxAlgorithm_P300RSVPSpellerStimulatorDesc OpenViBE::CIdentifier(0x8B0D160B, 0xCAB2E383)

namespace OpenViBEPlugins
{
	namespace Stimulation
	{
		class CBoxAlgorithmP300RSVPSpellerStimulator : public OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual OpenViBE::uint64 getClockFrequency(void);
			virtual OpenViBE::boolean initialize(void);
			virtual OpenViBE::boolean uninitialize(void);
			virtual OpenViBE::boolean processInput(OpenViBE::uint32 ui32Index);
			virtual OpenViBE::boolean processClock(OpenViBE::CMessageClock& rMessageClock);
			virtual OpenViBE::boolean process(void);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_P300RSVPSpellerStimulator);

		protected:

			OpenViBE::uint64 m_ui64StartStimulation;
			OpenViBE::uint64 m_ui64CharStimulationBase;

			OpenViBE::uint64 m_ui64CharsCount;
			OpenViBE::uint64 m_ui64SequenceRepetitions;	

			OpenViBE::uint64 m_ui64RepetitionCountInTrial;
			OpenViBE::uint64 m_ui64TrialCount;
			OpenViBE::uint64 m_ui64LetterDuration;
			OpenViBE::uint64 m_ui64InterRepetitionDuration;
			OpenViBE::uint64 m_ui64InterTrialDuration;

			OpenViBE::boolean m_bAvoidNeighborFlashing;

		private:

			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pStimulationDecoder;
			OpenViBE::uint64 m_ui64LastTime;
			OpenViBE::boolean m_bHeaderSent;
			OpenViBE::boolean m_bStartReceived;

			OpenViBE::uint32 m_ui32LastState;
			OpenViBE::uint64 m_ui64TrialStartTime;

			OpenViBE::uint64 m_ui64FlashCountInRepetition;
			OpenViBE::uint64 m_ui64RepetitionDuration;
			OpenViBE::uint64 m_ui64TrialDuration;
			OpenViBE::uint64 m_ui64TrialIndex;

			std::map < OpenViBE::uint64, OpenViBE::uint64 > m_vChar;

		private:

			void generate_sequence(void);
			int getASCII(int number);
			void generate_simple_sequence(void);
		};

		class CBoxAlgorithmP300RSVPSpellerStimulatorDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("P300 Speller Stimulator"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Yann Renard"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("INRIA"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Generates a stimulation sequence suitable for a P300 speller"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString(""); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Stimulation"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-select-font"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_P300RSVPSpellerStimulator; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::Stimulation::CBoxAlgorithmP300RSVPSpellerStimulator; }

			virtual OpenViBE::boolean getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput  ("Incoming stimulations",           OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addOutput ("Produced stimulations",           OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Start stimulation",               OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
				rBoxAlgorithmPrototype.addSetting("Char stimulation base",            OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");

				rBoxAlgorithmPrototype.addSetting("Number of trials",                OV_TypeId_Integer,     "5");
				rBoxAlgorithmPrototype.addSetting("Number of sequence",                OV_TypeId_Integer,     "10");
				rBoxAlgorithmPrototype.addSetting("Letter duration (in sec)",         OV_TypeId_Float,       "0.075");
				rBoxAlgorithmPrototype.addSetting("Inter-repetition delay (in sec)", OV_TypeId_Float,       "2");
				rBoxAlgorithmPrototype.addSetting("Inter-trial delay (in sec)",      OV_TypeId_Float,       "5");


				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_P300RSVPSpellerStimulatorDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_P300RSVPSpellerStimulator_H__
