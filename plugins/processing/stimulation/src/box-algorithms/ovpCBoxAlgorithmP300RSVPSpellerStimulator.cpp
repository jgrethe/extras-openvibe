//
// @todo the code logic in process() should be simplified and made more clear, perhaps by adding more states to the machine
//
#include "ovpCBoxAlgorithmP300RSVPSpellerStimulator.h"

#include <list>
#include <cstdlib>

#include <openvibe/ovITimeArithmetics.h>

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::Stimulation;

#define _LOG_(lm, x) { lm << x; }
#define _OPTIONAL_LOG_(lm, x) /* _LOG_(lm, x); */

namespace
{
	enum
	{
		State_None,
		State_Show,
		State_Next,
		State_RepetitionRest,
		State_TrialRest,
		State_ExperimentStop
	};
};

uint64 CBoxAlgorithmP300RSVPSpellerStimulator::getClockFrequency(void)
{
	return 128LL<<32;
}

boolean CBoxAlgorithmP300RSVPSpellerStimulator::initialize(void)
{
	const IBox& l_rStaticBoxContext = this->getStaticBoxContext();

	m_pStimulationDecoder= nullptr;
	m_pStimulationEncoder= nullptr;

	m_ui64StartStimulation      = this->getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_Stimulation, FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0));
	m_ui64CharStimulationBase 	= this->getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_Stimulation, FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1));

	m_ui64CharsCount = 30;	// Can be getted from settings: m_ui64ColumnCount = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 4);

	if(m_ui64CharsCount==0)
	{
        _LOG_(this->getLogManager(), LogLevel_ImportantWarning << "This stimulator should at least have 1 character (got " << m_ui64CharsCount << "\n");
		return false;
	}

	m_ui64TrialCount = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	m_ui64SequenceRepetitions =  FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);
	m_ui64LetterDuration = ITimeArithmetics::secondsToTime((float64)FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 4));
	m_ui64InterRepetitionDuration = ITimeArithmetics::secondsToTime((float64)FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 5));
	m_ui64InterTrialDuration = ITimeArithmetics::secondsToTime((float64)FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 6));


	const uint64 l_ui64DurationThreshold = (10LL<<32)/1000;	// 10ms
	if(m_ui64InterRepetitionDuration<l_ui64DurationThreshold)
	{
		_LOG_(this->getLogManager(), LogLevel_Warning << "Inter repetition duration should not be less than 10 ms\n");
		m_ui64InterRepetitionDuration=l_ui64DurationThreshold;
	}

	if(m_ui64InterTrialDuration<l_ui64DurationThreshold)
	{
		_LOG_(this->getLogManager(), LogLevel_Warning << "Inter trial duration should not be less than 10 ms\n");
		m_ui64InterTrialDuration=l_ui64DurationThreshold;
	}


	// ----------------------------------------------------------------------------------------------------------------------------------------------------------

	m_pStimulationEncoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamEncoder));
	m_pStimulationEncoder->initialize();

	m_pStimulationDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
	m_pStimulationDecoder->initialize();

	m_ui64LastTime=0;
	m_bHeaderSent=false;
	m_bStartReceived=false;

	m_ui32LastState=State_None;
	m_ui64TrialStartTime=m_ui64InterTrialDuration;

	m_ui64RepetitionDuration= m_ui64CharsCount*m_ui64LetterDuration;
	m_ui64TrialDuration=m_ui64SequenceRepetitions*(m_ui64RepetitionDuration+m_ui64InterRepetitionDuration);
	m_ui64TrialIndex=1;

	this->generate_sequence();
	return true;
}

boolean CBoxAlgorithmP300RSVPSpellerStimulator::uninitialize(void)
{
	if(m_pStimulationDecoder)
	{
		m_pStimulationDecoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationDecoder);
		m_pStimulationDecoder= nullptr;
	}

	if(m_pStimulationEncoder)
	{
		m_pStimulationEncoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pStimulationEncoder);
		m_pStimulationEncoder= nullptr;
	}

	return true;
}

boolean CBoxAlgorithmP300RSVPSpellerStimulator::processInput(uint32 ui32InputIndex)
{
	// IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();

	for(uint32 i=0; i<l_rDynamicBoxContext.getInputChunkCount(0); i++)
	{
		if(!m_bStartReceived)
		{
			TParameterHandler < const IMemoryBuffer* > ip_pMemoryBuffer(m_pStimulationDecoder->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
			TParameterHandler < IStimulationSet* > op_pStimulationSet(m_pStimulationDecoder->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));
			ip_pMemoryBuffer=l_rDynamicBoxContext.getInputChunk(0, i);
			m_pStimulationDecoder->process();

			if(m_pStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedHeader))
			{
			}

			if(m_pStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
			{
				for(uint32 j=0; j<op_pStimulationSet->getStimulationCount(); j++)
				{
					if(op_pStimulationSet->getStimulationIdentifier(j) == m_ui64StartStimulation)
					{
						m_ui64TrialStartTime=op_pStimulationSet->getStimulationDate(j)+m_ui64InterTrialDuration;
						m_bStartReceived=true;
					}
				}
			}

			if(m_pStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedEnd))
			{
			}
		}

		l_rDynamicBoxContext.markInputAsDeprecated(0, i);
	}

	return true;
}

boolean CBoxAlgorithmP300RSVPSpellerStimulator::processClock(IMessageClock& rMessageClock)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

boolean CBoxAlgorithmP300RSVPSpellerStimulator::process(void)
{
	// IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();

    uint32 l_ui32State=State_None;
	uint64 l_ui64CurrentTime=this->getPlayerContext().getCurrentTime();
	uint64 l_ui64ShowIndex=(uint64)-1;
	// FIXME is it necessary to keep next line uncomment ?
	//uint64 l_ui64RepetitionIndex=(uint64)-1;

	CStimulationSet l_oStimulationSet;

	if(m_bStartReceived)
	{
		if(l_ui64CurrentTime<m_ui64TrialStartTime)
		{
			l_ui32State=State_TrialRest;
		}
		else
		{
			if ((m_ui64TrialIndex>m_ui64TrialCount) && (m_ui64TrialCount>0)) 
			{
				l_ui32State=State_ExperimentStop;
			}
			else
			{
				uint64 l_ui64CurrentTimeInTrial     =l_ui64CurrentTime-m_ui64TrialStartTime;
				uint64 l_ui64CurrentTimeInRepetition=l_ui64CurrentTimeInTrial%(m_ui64RepetitionDuration+m_ui64InterRepetitionDuration);
				
				// FIXME is it necessary to keep next line uncomment ?
				//uint64 l_ui64RepritionIndexInTrial  =l_ui64CurrentTimeInTrial/(m_ui64RepetitionDuration+m_ui64InterRepetitionDuration);
				uint64 l_ui64ShowIndexInRepetition =l_ui64CurrentTimeInRepetition/(m_ui64LetterDuration);
				
				l_ui64ShowIndex=l_ui64ShowIndexInRepetition;
				// FIXME is it necessary to keep next line uncomment ?
				//l_ui64RepetitionIndex=l_ui64RepritionIndexInTrial;
				
				if(l_ui64CurrentTimeInTrial >= m_ui64TrialDuration)
				{
					if(m_ui64TrialCount==0 || m_ui64TrialIndex<=m_ui64TrialCount)
					{					
						m_ui64TrialStartTime=l_ui64CurrentTime+m_ui64InterTrialDuration;					
						l_ui32State=State_TrialRest;
						l_ui64ShowIndex=(uint64)-1;
						// FIXME is it necessary to keep next line uncomment ?
						//l_ui64RepetitionIndex=(uint64)-1;
						m_ui64TrialIndex++;
					}
					else
					{
						m_ui64TrialStartTime=l_ui64CurrentTime+m_ui64InterTrialDuration;
						l_ui32State=State_None;
					}
				}
				else
				{
					if(l_ui64CurrentTimeInRepetition >= m_ui64RepetitionDuration)
					{
						l_ui32State=State_RepetitionRest;
						l_ui64ShowIndex=(uint64)-1;
					}
					else
					{
						//TODO: revisar estos cambios de estado
						if(l_ui64CurrentTimeInRepetition%(m_ui64LetterDuration)<m_ui64LetterDuration/2)
						{
                            l_ui32State=State_Show;
						}
						else
						{
                            l_ui32State=State_Next;
						}
					}
				}
			}
		}

		if(l_ui32State!=m_ui32LastState)
		{
			boolean l_bChar=(l_ui32State==1?true:false);
            long l_iChar;
            if(l_bChar)
            {
                l_iChar=m_vChar[l_ui64ShowIndex];
                l_ui64ShowIndex++;
            }else{
                l_iChar=-1;
            }
			switch(m_ui32LastState)
			{
				case State_Show:
					l_oStimulationSet.appendStimulation(OVTK_StimulationId_VisualStimulationStop, l_ui64CurrentTime, 0);
					_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_VisualStimulationStop\n");
					break;
				
				case State_Next:
					break;

				case State_RepetitionRest:
					if(l_ui32State!=State_TrialRest && l_ui32State!=State_None)
					{
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_SegmentStart, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_SegmentStart\n");
					}
					break;

				case State_TrialRest:
					if (m_ui64TrialIndex<=m_ui64TrialCount)
					{
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_RestStop, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_RestStop\n");
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_TrialStart, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_TrialStart\n");
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_SegmentStart, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_SegmentStart\n");
					}
					break;

				case State_None:
					if (m_ui64TrialIndex<=m_ui64TrialCount)
					{
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_ExperimentStart, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_ExperimentStart\n");
					}
					break;

				default:
					break;
			}

			switch(l_ui32State)
			{
				case State_Show:
					l_oStimulationSet.appendStimulation(m_ui64CharStimulationBase+l_iChar, l_ui64CurrentTime, 0);
					_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_LabelId(x)\n");
					l_oStimulationSet.appendStimulation(OVTK_StimulationId_VisualStimulationStart, l_ui64CurrentTime, 0);
					_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_VisualStimulationStart\n");
					break;

				 case State_Next:
				 	break;

				case State_RepetitionRest:
					l_oStimulationSet.appendStimulation(OVTK_StimulationId_SegmentStop, l_ui64CurrentTime, 0);
					_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_SegmentStop\n");
					this->generate_sequence();
					break;

				case State_TrialRest:
					if(m_ui32LastState!=State_None)
					{
						if(m_ui32LastState!=State_RepetitionRest)
						{
							l_oStimulationSet.appendStimulation(OVTK_StimulationId_SegmentStop, l_ui64CurrentTime, 0);
							_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_SegmentStop\n");
						}
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_TrialStop, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_TrialStop\n");
					}
					if (m_ui64TrialIndex<=m_ui64TrialCount)
					{
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_RestStart, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_RestStart\n");
					}
					break;

				case State_None:
					if(m_ui32LastState!=State_RepetitionRest)
					{
						l_oStimulationSet.appendStimulation(OVTK_StimulationId_SegmentStop, l_ui64CurrentTime, 0);
						_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_SegmentStop\n");
					}
					l_oStimulationSet.appendStimulation(OVTK_StimulationId_TrialStop, l_ui64CurrentTime, 0);
					_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_TrialStop\n");					
					break;
				case State_ExperimentStop:
					// The experiment stop is sent with some delay to allow the last flash / letter to be processed gracefully by the DSP later
					l_oStimulationSet.appendStimulation(OVTK_StimulationId_ExperimentStop, l_ui64CurrentTime + ITimeArithmetics::secondsToTime(3.0), 0);
					_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Trace << "sends OVTK_StimulationId_ExperimentStop\n");
					break;			

				default:
					break;
			}

			m_ui32LastState=l_ui32State;
		}

// FIXME is it necessary to keep next code uncomment ?
#if 0
		_OPTIONAL_LOG_(this->getLogManager(), LogLevel_Info << "State:" << state_to_string(l_ui32State) << " - flash index:" << l_ui64ShowIndex << " - repetition index:" << l_ui64RepetitionIndex << " - trial index:" << m_ui64TrialIndex << "\n");
#endif
	}

	TParameterHandler < IStimulationSet* > ip_pStimulationSet(m_pStimulationEncoder->getInputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_InputParameterId_StimulationSet));
	TParameterHandler < IMemoryBuffer* > op_pMemoryBuffer(m_pStimulationEncoder->getOutputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_OutputParameterId_EncodedMemoryBuffer));
	ip_pStimulationSet=&l_oStimulationSet;
	op_pMemoryBuffer=l_rDynamicBoxContext.getOutputChunk(0);
	if(!m_bHeaderSent)
	{
		m_pStimulationEncoder->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeHeader);
		l_rDynamicBoxContext.markOutputAsReadyToSend(0, m_ui64LastTime, l_ui64CurrentTime);
	}
	if(m_ui64LastTime!=l_ui64CurrentTime)
	{
		m_pStimulationEncoder->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeBuffer);
		l_rDynamicBoxContext.markOutputAsReadyToSend(0, m_ui64LastTime, l_ui64CurrentTime);
	}
	m_ui64LastTime=l_ui64CurrentTime;
	m_bHeaderSent=true;

	return true;
}

// A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  .  !  _  ⌫	
// 0  1  2  3  4  5  6 ...								

void CBoxAlgorithmP300RSVPSpellerStimulator::generate_sequence(void)
{
	this->generate_simple_sequence();
	for (int i = 0; i < m_ui64CharsCount; i++)
	{
		for (int j = 1; j < m_ui64SequenceRepetitions; j++)
		{
			m_vChar[i+j*m_ui64CharsCount]=m_vChar[i];
		}	
	}
}

int CBoxAlgorithmP300RSVPSpellerStimulator::getASCII(int number)
{
	if(number >= 0 && number < 26 )
	{
		return 65 + number;
	} 
	else if (number == 26)
	{
		return 46; // .
	}
	else if (number == 27)
	{
		return 33; // !
	}
	else if (number == 28)
	{
		return 95; //_
	}
	else if (number == 29)
	{
		return 60; //Backspace '<' representative char 
	}
	else
	{
		// Error: Invalid number
	}
}

void CBoxAlgorithmP300RSVPSpellerStimulator::generate_simple_sequence(void)
{
	uint32 i,j;
	std::vector < uint32 > l_vChar;
	m_vChar.clear();
	for(i=0; i<m_ui64CharsCount; i++)
	{
		l_vChar.push_back(i);
	}
	for(i=0; i<m_ui64CharsCount; i++)
	{
		j=rand()%l_vChar.size();
		m_vChar[i]=this->getASCII(l_vChar[j]);
		l_vChar.erase(l_vChar.begin()+j);
	}
}
