
// @todo for clarity, the StimulusSender related code blocks should be pushed inside the class and away from here

#include "ovpCBoxAlgorithmP300RSVPSpellerVisualization.h"

#include <system/ovCMemory.h>

#include <list>
#include <vector>
#include <string>
#include <algorithm>

#include <tcptagging/IStimulusSender.h>

using namespace OpenViBE;
using namespace OpenViBE::Kernel;
using namespace OpenViBE::Plugins;

using namespace OpenViBEPlugins;
using namespace OpenViBEPlugins::SimpleVisualization;

char a = 'a';
char b = 'b';
bool s = true;

namespace
{
	class _AutoCast_
	{
	public:
		_AutoCast_(const IBox& rBox, IConfigurationManager& rConfigurationManager, const uint32 ui32Index)
			: m_rConfigurationManager(rConfigurationManager)
		{
			rBox.getSettingValue(ui32Index, m_sSettingValue);
			m_sSettingValue = m_rConfigurationManager.expand(m_sSettingValue);
		}
		operator ::GdkColor (void)
		{
			::GdkColor l_oColor;
			int r=0, g=0, b=0;
			sscanf(m_sSettingValue.toASCIIString(), "%i,%i,%i", &r, &g, &b);
			l_oColor.pixel=0;
			l_oColor.red=(r*65535)/100;
			l_oColor.green=(g*65535)/100;
			l_oColor.blue=(b*65535)/100;
			return l_oColor;
		}
	protected:
		IConfigurationManager& m_rConfigurationManager;
		CString m_sSettingValue;
	};

	static void toggle_button_show_hide_cb(::GtkToggleToolButton* pToggleButton, gpointer pUserData)
	{
		if(gtk_toggle_tool_button_get_active(pToggleButton))
		{
			gtk_widget_show(GTK_WIDGET(pUserData));
		}
		else
		{
			gtk_widget_hide(GTK_WIDGET(pUserData));
		}
	}
};

// This callback flushes all accumulated stimulations to the TCP Tagging 
// after the rendering has completed.
gboolean flush_callback2(gpointer pUserData)
{
	((CBoxAlgorithmP300RSVPSpellerVisualization*)pUserData)->flushQueue();
	
	return false;	// Only run once
}

bool CBoxAlgorithmP300RSVPSpellerVisualization::initialize(void)
{
	const IBox& l_rStaticBoxContext=this->getStaticBoxContext();

	m_pMainWidgetInterface= nullptr;
	m_pToolbarWidgetInterface= nullptr;
	m_pTargetFontDescription= nullptr;
	m_pSelectedFontDescription= nullptr;

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------

	m_sInterfaceFilename         =FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_ui64CharStimulationBase     =FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_oTargetBackgroundColor     =_AutoCast_(l_rStaticBoxContext, this->getConfigurationManager(), 2);
	m_oSelectedBackgroundColor   =_AutoCast_(l_rStaticBoxContext, this->getConfigurationManager(), 3);

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------

	m_pSequenceStimulationDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
	m_pSequenceStimulationDecoder->initialize();

	m_pTargetStimulationDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
	m_pTargetStimulationDecoder->initialize();

	m_pTargetFlaggingStimulationEncoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamEncoder));
	m_pTargetFlaggingStimulationEncoder->initialize();

	m_pCharSelectionStimulationDecoder=&this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationStreamDecoder));
	m_pCharSelectionStimulationDecoder->initialize();

	ip_pSequenceMemoryBuffer.initialize(m_pSequenceStimulationDecoder->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
	op_pSequenceStimulationSet.initialize(m_pSequenceStimulationDecoder->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));

	ip_pTargetMemoryBuffer.initialize(m_pTargetStimulationDecoder->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
	op_pTargetStimulationSet.initialize(m_pTargetStimulationDecoder->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));

	ip_pTargetFlaggingStimulationSet.initialize(m_pTargetFlaggingStimulationEncoder->getInputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_InputParameterId_StimulationSet));
	op_pTargetFlaggingMemoryBuffer.initialize(m_pTargetFlaggingStimulationEncoder->getOutputParameter(OVP_GD_Algorithm_StimulationStreamEncoder_OutputParameterId_EncodedMemoryBuffer));

	m_ui64LastTime=0;

	m_pStimulusSender = nullptr;

	m_uiIdleFuncTag = 0;
	m_vStimuliQueue.clear();

	m_pMainWidgetInterface=gtk_builder_new(); // glade_xml_new(m_sInterfaceFilename.toASCIIString(), "p300-speller-main", NULL);
	if(!gtk_builder_add_from_file(m_pMainWidgetInterface, m_sInterfaceFilename.toASCIIString(), NULL))
	{
		this->getLogManager() << LogLevel_ImportantWarning << "Could not load interface file [" << m_sInterfaceFilename << "]\n";
		this->getLogManager() << LogLevel_ImportantWarning << "The file may be missing. However, the interface files now use gtk-builder instead of glade. Did you update your files ?\n";
		return false;
	}

	m_pToolbarWidgetInterface=gtk_builder_new(); // glade_xml_new(m_sInterfaceFilename.toASCIIString(), "p300-speller-toolbar", NULL);
	gtk_builder_add_from_file(m_pToolbarWidgetInterface, m_sInterfaceFilename.toASCIIString(), NULL);

	m_pMainWindow=GTK_WIDGET(gtk_builder_get_object(m_pMainWidgetInterface, "p300-speller-main"));
	m_pToolbarWidget=GTK_WIDGET(gtk_builder_get_object(m_pToolbarWidgetInterface, "p300-speller-toolbar"));
	m_pTable=GTK_TABLE(gtk_builder_get_object(m_pMainWidgetInterface, "p300-speller-table"));
	m_pResult=GTK_LABEL(gtk_builder_get_object(m_pMainWidgetInterface, "label-result"));
	m_pTarget=GTK_LABEL(gtk_builder_get_object(m_pMainWidgetInterface, "label-target"));
	m_pMainLetterLabel=GTK_LABEL(gtk_builder_get_object(m_pMainWidgetInterface, "main-letter-label"));

    gtk_builder_connect_signals(m_pMainWidgetInterface, NULL);
	gtk_builder_connect_signals(m_pToolbarWidgetInterface, NULL);

	g_signal_connect(gtk_builder_get_object(m_pToolbarWidgetInterface, "toolbutton-show_target_text"),             "toggled", G_CALLBACK(toggle_button_show_hide_cb), gtk_builder_get_object(m_pMainWidgetInterface, "label-target"));
	g_signal_connect(gtk_builder_get_object(m_pToolbarWidgetInterface, "toolbutton-show_target_text"),             "toggled", G_CALLBACK(toggle_button_show_hide_cb), gtk_builder_get_object(m_pMainWidgetInterface, "label-target-title"));
	g_signal_connect(gtk_builder_get_object(m_pToolbarWidgetInterface, "toolbutton-show_result_text"),             "toggled", G_CALLBACK(toggle_button_show_hide_cb), gtk_builder_get_object(m_pMainWidgetInterface, "label-result"));
	g_signal_connect(gtk_builder_get_object(m_pToolbarWidgetInterface, "toolbutton-show_result_text"),             "toggled", G_CALLBACK(toggle_button_show_hide_cb), gtk_builder_get_object(m_pMainWidgetInterface, "label-result-title"));

	m_visualizationContext = dynamic_cast<OpenViBEVisualizationToolkit::IVisualizationContext*>(this->createPluginObject(OVP_ClassId_Plugin_VisualizationContext));
	m_visualizationContext->setWidget(*this, m_pMainWindow);
	m_visualizationContext->setToolbar(*this, m_pToolbarWidget);

	guint l_uiCharCount=0;
	g_object_get(m_pTable, "n-Chars", &l_uiCharCount, NULL);

	m_ui64CharCount=200;

	::PangoFontDescription* l_pMaxFontDescription=pango_font_description_copy(pango_context_get_font_description(gtk_widget_get_pango_context(m_pMainWindow)));
	m_pTargetFontDescription=pango_font_description_copy(pango_context_get_font_description(gtk_widget_get_pango_context(m_pMainWindow)));
	m_pSelectedFontDescription=pango_font_description_copy(pango_context_get_font_description(gtk_widget_get_pango_context(m_pMainWindow)));

    PangoAttrList *attrlist = pango_attr_list_new();
    PangoAttribute *attr = pango_attr_size_new_absolute(100 * PANGO_SCALE);
    pango_attr_list_insert(attrlist, attr);
    gtk_label_set_attributes(m_pMainLetterLabel, attrlist);
    pango_attr_list_unref(attrlist);

	uint64 l_ui64MaxSize=0;
	l_ui64MaxSize=std::max(l_ui64MaxSize, m_ui64TargetFontSize);
	l_ui64MaxSize=std::max(l_ui64MaxSize, m_ui64SelectedFontSize);

	pango_font_description_set_size(l_pMaxFontDescription, (gint)(l_ui64MaxSize * PANGO_SCALE));
	pango_font_description_set_size(m_pTargetFontDescription, (gint)(m_ui64TargetFontSize * PANGO_SCALE));
	pango_font_description_set_size(m_pSelectedFontDescription, (gint)(m_ui64SelectedFontSize * PANGO_SCALE));


    this->_cache_build_from_table_(m_pTable);


	pango_font_description_free(l_pMaxFontDescription);

	m_iLastTargetChar=-1;
	m_iTargetChar=-1;
	m_iSelectedChar=-1;

	m_pStimulusSender = TCPTagging::createStimulusSender();

	if(!m_pStimulusSender->connect("localhost", "15361"))
	{
		this->getLogManager() << LogLevel_Warning << "Unable to connect to AS TCP Tagging, stimuli wont be forwarded.\n";
	}

	m_bTableInitialized=false;

	return true;
}

bool CBoxAlgorithmP300RSVPSpellerVisualization::uninitialize(void)
{
	if(m_uiIdleFuncTag)
	{
		m_vStimuliQueue.clear();
		g_source_remove(m_uiIdleFuncTag);
		m_uiIdleFuncTag = 0;
	}

	if(m_pStimulusSender)
	{
		delete m_pStimulusSender;
		m_pStimulusSender = nullptr;
	}

	if(m_pSelectedFontDescription)
	{
		pango_font_description_free(m_pSelectedFontDescription);
		m_pSelectedFontDescription= nullptr;
	}

	if(m_pTargetFontDescription)
	{
		pango_font_description_free(m_pTargetFontDescription);
		m_pTargetFontDescription= nullptr;
	}

	if(m_pToolbarWidgetInterface)
	{
		g_object_unref(m_pToolbarWidgetInterface);
		m_pToolbarWidgetInterface= nullptr;
	}

	if(m_pMainWidgetInterface)
	{
		g_object_unref(m_pMainWidgetInterface);
		m_pMainWidgetInterface= nullptr;
	}

	ip_pTargetFlaggingStimulationSet.uninitialize();
	op_pTargetFlaggingMemoryBuffer.uninitialize();

	op_pTargetStimulationSet.uninitialize();
	ip_pTargetMemoryBuffer.uninitialize();

	op_pSequenceStimulationSet.uninitialize();
	ip_pSequenceMemoryBuffer.uninitialize();

	if(m_pCharSelectionStimulationDecoder)
	{
		m_pCharSelectionStimulationDecoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pCharSelectionStimulationDecoder);
		m_pCharSelectionStimulationDecoder= nullptr;
	}

	if(m_pTargetFlaggingStimulationEncoder)
	{
		m_pTargetFlaggingStimulationEncoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pTargetFlaggingStimulationEncoder);
		m_pTargetFlaggingStimulationEncoder= nullptr;
	}

	if(m_pTargetStimulationDecoder)
	{
		m_pTargetStimulationDecoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pTargetStimulationDecoder);
		m_pTargetStimulationDecoder= nullptr;
	}

	if(m_pSequenceStimulationDecoder)
	{
		m_pSequenceStimulationDecoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_pSequenceStimulationDecoder);
		m_pSequenceStimulationDecoder= nullptr;
	}

	if(m_visualizationContext)
	{
		this->releasePluginObject(m_visualizationContext);
		m_pSequenceStimulationDecoder=nullptr;
	}

	return true;
}

bool CBoxAlgorithmP300RSVPSpellerVisualization::processInput(uint32 ui32Index)
{
	this->getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmP300RSVPSpellerVisualization::process(void)
{
	// IBox& l_rStaticBoxContext=this->getStaticBoxContext();
	IBoxIO& l_rDynamicBoxContext=this->getDynamicBoxContext();

	uint32 i, j, k;

	// --- Sequence stimulations

	for(i=0; i<l_rDynamicBoxContext.getInputChunkCount(0); i++)
	{

		CStimulationSet l_oFlaggingStimulationSet;

		ip_pSequenceMemoryBuffer=l_rDynamicBoxContext.getInputChunk(0, i);
		ip_pTargetFlaggingStimulationSet=&l_oFlaggingStimulationSet;
		op_pTargetFlaggingMemoryBuffer=l_rDynamicBoxContext.getOutputChunk(0);

		m_pSequenceStimulationDecoder->process();

		m_ui64LastTime=l_rDynamicBoxContext.getInputChunkEndTime(0, i);

		if(m_pSequenceStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedHeader))
		{
			m_pTargetFlaggingStimulationEncoder->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeHeader);
		}

		if(m_pSequenceStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
		{
			IStimulationSet* l_pStimulationSet=op_pSequenceStimulationSet;
			for(j=0; j<l_pStimulationSet->getStimulationCount(); j++)
			{
				uint64 l_ui64StimulationIdentifier=l_pStimulationSet->getStimulationIdentifier(j);
				char l_iChar=-1;
				bool l_bIsTarget = false;
				std::vector < ::GtkWidget* > l_vWidgets;
                l_iChar=(char)(l_ui64StimulationIdentifier-m_ui64CharStimulationBase );
                char aux = (char) m_iLastTargetChar;
                aux = toupper(aux);
                l_bIsTarget = (l_iChar==aux);

				if(l_ui64StimulationIdentifier == OVTK_StimulationId_Reset)
				{
					gtk_label_set_text(m_pTarget, "");
					gtk_label_set_text(m_pResult, "");
				}

				// Pass the stimulation to the server also as-is. If its a flash, it can be differentiated from a 'target' spec because
				// its NOT between OVTK_StimulationId_RestStart and OVTK_StimulationId_RestStop stimuli in the generated P300 timeline.
				m_vStimuliQueue.push_back(l_ui64StimulationIdentifier);

				if(l_ui64StimulationIdentifier >= m_ui64CharStimulationBase && l_ui64StimulationIdentifier < m_ui64CharStimulationBase+m_ui64CharCount)
				{
					std::string aux(1, l_iChar);

					gtk_label_set_text(m_pMainLetterLabel, (aux).c_str());	
					if(l_bIsTarget)
					{
						m_vStimuliQueue.push_back(OVTK_StimulationId_Target);
						l_oFlaggingStimulationSet.appendStimulation(OVTK_StimulationId_Target, l_pStimulationSet->getStimulationDate(j), 0);
					}
					else
					{
						m_vStimuliQueue.push_back(OVTK_StimulationId_NonTarget);
						l_oFlaggingStimulationSet.appendStimulation(OVTK_StimulationId_NonTarget, l_pStimulationSet->getStimulationDate(j), 0);
					}				
				}



			}
			m_pTargetFlaggingStimulationEncoder->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeBuffer);
		}

		if(m_pSequenceStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedEnd))
		{
			m_pTargetFlaggingStimulationEncoder->process(OVP_GD_Algorithm_StimulationStreamEncoder_InputTriggerId_EncodeEnd);
		}

		l_rDynamicBoxContext.markInputAsDeprecated(0, i);
		l_rDynamicBoxContext.markOutputAsReadyToSend(0, l_rDynamicBoxContext.getInputChunkStartTime(0, i), l_rDynamicBoxContext.getInputChunkEndTime(0, i));
	}

	// --- Target stimulations

	for(i=0; i<l_rDynamicBoxContext.getInputChunkCount(1); i++)
	{
		if(m_ui64LastTime>=l_rDynamicBoxContext.getInputChunkStartTime(1, i))
		{
			ip_pTargetMemoryBuffer=l_rDynamicBoxContext.getInputChunk(1, i);
			m_pTargetStimulationDecoder->process();

			if(m_pTargetStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedHeader))
			{
			}

			if(m_pTargetStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
			{
				IStimulationSet* l_pStimulationSet=op_pTargetStimulationSet;
				for(j=0; j<l_pStimulationSet->getStimulationCount(); j++)
				{
					uint64 l_ui64StimulationIdentifier=l_pStimulationSet->getStimulationIdentifier(j);	

                    this->getLogManager() << LogLevel_Debug << "Received Target Char " << l_ui64StimulationIdentifier << "\n";
                    m_iTargetChar=(char)(l_ui64StimulationIdentifier);

                    this->getLogManager() << LogLevel_Debug << "Displays Target Cell\n";

                    // Merge the current target into the stimulation stream. It can be differentiated
                    // from a 'flash' spec because it IS between OVTK_StimulationId_RestStart and
                    // OVTK_StimulationId_RestStop stimulations in the P300 timeline.
                    {
                        m_vStimuliQueue.push_back(m_iTargetChar);
                    }


                    std::string l_sString;
                    l_sString=gtk_label_get_text(m_pTarget);
                    std::string s1; s1=(char) m_iTargetChar;
                    l_sString += s1;
                    gtk_label_set_text(m_pTarget, l_sString.c_str());

                    m_vTargetHistory.push_back(m_iTargetChar);
                    m_iLastTargetChar=m_iTargetChar;
                    m_iTargetChar=-1;

				}
			}

			if(m_pTargetStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedEnd))
			{
			}

			l_rDynamicBoxContext.markInputAsDeprecated(1, i);
		}
	}

	// --- Selection stimulations

	for(k=2; k<4; k++)
	{
		IAlgorithmProxy* l_pSelectionStimulationDecoder=m_pCharSelectionStimulationDecoder;
		TParameterHandler < const IMemoryBuffer* > ip_pSelectionMemoryBuffer(l_pSelectionStimulationDecoder->getInputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_InputParameterId_MemoryBufferToDecode));
		TParameterHandler < IStimulationSet* > op_pSelectionStimulationSet(l_pSelectionStimulationDecoder->getOutputParameter(OVP_GD_Algorithm_StimulationStreamDecoder_OutputParameterId_StimulationSet));

		for(i=0; i<l_rDynamicBoxContext.getInputChunkCount(k); i++)
		{
			if(m_ui64LastTime>=l_rDynamicBoxContext.getInputChunkStartTime(k, i))
			{
				ip_pSelectionMemoryBuffer=l_rDynamicBoxContext.getInputChunk(k, i);
				l_pSelectionStimulationDecoder->process();

				if(l_pSelectionStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedHeader))
				{
				}

				if(l_pSelectionStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedBuffer))
				{
					IStimulationSet* l_pStimulationSet=op_pSelectionStimulationSet;
					for(j=0; j<l_pStimulationSet->getStimulationCount(); j++)
					{
						uint64 l_ui64StimulationIdentifier=l_pStimulationSet->getStimulationIdentifier(j);
						bool l_bSelected=false;
						std::vector < ::GtkWidget* > l_vWidgets;

						if(l_ui64StimulationIdentifier >= m_ui64CharStimulationBase && l_ui64StimulationIdentifier < m_ui64CharStimulationBase+m_ui64CharCount)
						{
							this->getLogManager() << LogLevel_Debug << "Received Selected Char " << l_ui64StimulationIdentifier << "\n";
							m_iSelectedChar=(int)(l_ui64StimulationIdentifier-m_ui64CharStimulationBase);
							l_bSelected=true;
						}
						if(l_ui64StimulationIdentifier == OVTK_StimulationId_Label_00)
						{
							if(k==2) m_iSelectedChar=-2;
							l_bSelected=true;
						}
						if(l_bSelected && m_iSelectedChar!=-1)
						{
							if(m_iSelectedChar>=0)
							{

								std::string l_sString;
								l_sString=(char) m_iSelectedChar;
								if(m_vTargetHistory.size())
								{
									std::list < int >::const_iterator it=m_vTargetHistory.begin();
                                    char comp = (char) *it;
									bool l_bCorrect=(comp==m_iSelectedChar);
									m_vTargetHistory.pop_front();
									if(l_bCorrect)
									{
										l_sString="<span color=\"darkgreen\">" + l_sString + "</span>";
									}
									else
									{
										l_sString="<span color=\"darkred\">" + l_sString + "</span>";
									}
								}
								l_sString=std::string(gtk_label_get_label(m_pResult))+l_sString;
								gtk_label_set_markup(m_pResult, l_sString.c_str());
	
							}
							else
							{
								this->getLogManager() << LogLevel_Trace << "Selection Rejected !\n";
								std::string l_sString;
								l_sString=gtk_label_get_text(m_pResult);
								l_sString+="*";
								gtk_label_set_text(m_pResult, l_sString.c_str());
							}

							m_iSelectedChar=-1;
						}
					}
				}

				if(l_pSelectionStimulationDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StimulationStreamDecoder_OutputTriggerId_ReceivedEnd))
				{
				}

				l_rDynamicBoxContext.markInputAsDeprecated(k, i);
			}
		}
	}

	// After any possible rendering, we flush the accumulated stimuli. The default idle func is low priority, so it should be run after rendering by gtk.
	if (m_uiIdleFuncTag == 0)
	{
		m_uiIdleFuncTag = g_idle_add(flush_callback2, this);
	}

	return true;
}

// _________________________________________________________________________________________________________________________________________________________
//

void CBoxAlgorithmP300RSVPSpellerVisualization::_cache_build_from_table_(::GtkTable* pTable)
{
	if(pTable)
	{
		::GdkColor l_oWhite;
		l_oWhite.red=65535;
		l_oWhite.green=65535;
		l_oWhite.blue=65535;
		l_oWhite.pixel=65535;

		::GtkTableChild* l_pTableChild= nullptr;
		::GList* l_pList= nullptr;
		for(l_pList=pTable->children; l_pList; l_pList=l_pList->next)
		{
			l_pTableChild=(::GtkTableChild*)l_pList->data;

			for(unsigned long i=l_pTableChild->top_attach; i<l_pTableChild->bottom_attach; i++)
			{
				for(unsigned long j=l_pTableChild->left_attach; j<l_pTableChild->right_attach; j++)
				{
					CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& l_rWidgetStyle=m_vCache[i][j];
					l_rWidgetStyle.pWidget=l_pTableChild->widget;
					l_rWidgetStyle.pChildWidget=gtk_bin_get_child(GTK_BIN(l_pTableChild->widget));
					l_rWidgetStyle.oBackgroundColor=l_oWhite;
					l_rWidgetStyle.oForegroundColor=l_oWhite;
					l_rWidgetStyle.pFontDescription= nullptr;
				}
			}
		}
	}
}

// Note that we don't need concurrency control here as gtk callbacks run in the main thread
void CBoxAlgorithmP300RSVPSpellerVisualization::flushQueue(void)
{
	for(size_t i=0;i<m_vStimuliQueue.size();i++)
	{
		m_pStimulusSender->sendStimulation(m_vStimuliQueue[i]);
	}
	m_vStimuliQueue.clear();

	// This function will be automatically removed after completion, so set to 0
	m_uiIdleFuncTag = 0;
}
