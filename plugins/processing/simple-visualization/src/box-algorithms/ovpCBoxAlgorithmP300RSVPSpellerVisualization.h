#ifndef __OpenViBEPlugins_BoxAlgorithm_P300RSVPSpellerVisualization_H__
#define __OpenViBEPlugins_BoxAlgorithm_P300RSVPSpellerVisualization_H__

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <gtk/gtk.h>
#include <map>
#include <list>

// TODO:
// - please move the identifier definitions in ovp_defines.h
// - please include your desciptor in ovp_main.cpp

#define OVP_ClassId_BoxAlgorithm_P300RSVPSpellerVisualization     OpenViBE::CIdentifier(0xFEDFF88E, 0xF6B25EC6)
#define OVP_ClassId_BoxAlgorithm_P300RSVPSpellerVisualizationDesc OpenViBE::CIdentifier(0x61AA0174, 0x82B4498E)


namespace TCPTagging
{
	class IStimulusSender; // fwd declare
};

namespace OpenViBEPlugins
{
	namespace SimpleVisualization
	{


		class CBoxAlgorithmP300RSVPSpellerVisualization : public OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >
		{
		public:

			virtual void release(void) { delete this; }

			virtual bool initialize(void);
			virtual bool uninitialize(void);
			virtual bool processInput(OpenViBE::uint32 ui32Index);
			virtual bool process(void);

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_P300RSVPSpellerVisualization);

		private:

			typedef struct
			{
				::GtkWidget* pWidget;
				::GtkWidget* pChildWidget;
				::GdkColor oBackgroundColor;
				::GdkColor oForegroundColor;
				::PangoFontDescription* pFontDescription;
			} SWidgetStyle;

			typedef void (CBoxAlgorithmP300RSVPSpellerVisualization::*_cache_callback_)(CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& rWidgetStyle, void* pUserData);

			void _cache_build_from_table_(::GtkTable* pTable);
			void _cache_for_each_(_cache_callback_ fpCallback, void* pUserData);
			void _cache_for_each_if_(int iLine, _cache_callback_ fpIfCallback, _cache_callback_ fpElseCallback, void* pIfUserData, void* pElseUserData);
			void _cache_change_null_cb_(CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_background_cb_(CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_foreground_cb_(CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_change_font_cb_(CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_collect_widget_cb_(CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& rWidgetStyle, void* pUserData);
			void _cache_collect_child_widget_cb_(CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle& rWidgetStyle, void* pUserData);

		public:

			void flushQueue(void);					// Sends all accumulated stimuli to the TCP Tagging

		protected:

			OpenViBE::CString m_sInterfaceFilename;
			OpenViBE::uint64 m_ui64CharStimulationBase;


			::GdkColor m_oTargetBackgroundColor;
			::GdkColor m_oSelectedBackgroundColor;
			OpenViBE::uint64 m_ui64TargetFontSize;
			::PangoFontDescription* m_pTargetFontDescription;
			::PangoFontDescription* m_pSelectedFontDescription;
			OpenViBE::uint64 m_ui64SelectedFontSize;

		private:

			OpenViBE::Kernel::IAlgorithmProxy* m_pSequenceStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pTargetStimulationDecoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pTargetFlaggingStimulationEncoder;
			OpenViBE::Kernel::IAlgorithmProxy* m_pCharSelectionStimulationDecoder;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pSequenceMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IMemoryBuffer*> ip_pTargetMemoryBuffer;
			OpenViBE::Kernel::TParameterHandler<const OpenViBE::IStimulationSet*> ip_pTargetFlaggingStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pSequenceStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IStimulationSet*> op_pTargetStimulationSet;
			OpenViBE::Kernel::TParameterHandler<OpenViBE::IMemoryBuffer*> op_pTargetFlaggingMemoryBuffer;
			OpenViBE::uint64 m_ui64LastTime;

			::GtkBuilder* m_pMainWidgetInterface;
			::GtkBuilder* m_pToolbarWidgetInterface;
			::GtkWidget* m_pMainWindow;
			::GtkWidget* m_pToolbarWidget;
			::GtkTable* m_pTable;
			::GtkLabel* m_pResult;
			::GtkLabel* m_pTarget;
			::GtkLabel* m_pMainLetterLabel;
			OpenViBE::uint64 m_ui64CharCount;

			int m_iLastTargetChar;
			int m_iTargetChar;
			int m_iSelectedChar;

			bool m_bTableInitialized;

			std::map < unsigned long, std::map < unsigned long, CBoxAlgorithmP300RSVPSpellerVisualization::SWidgetStyle > > m_vCache;
			std::list < int > m_vTargetHistory;

			// TCP Tagging
			std::vector< OpenViBE::uint64 > m_vStimuliQueue;
			guint m_uiIdleFuncTag;
			TCPTagging::IStimulusSender* m_pStimulusSender;

			OpenViBEVisualizationToolkit::IVisualizationContext* m_visualizationContext = nullptr;
		};

		class CBoxAlgorithmP300RSVPSpellerVisualizationDesc : public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			virtual void release(void) { }

			virtual OpenViBE::CString getName(void) const                { return OpenViBE::CString("P300 RSVP Speller Visualization"); }
			virtual OpenViBE::CString getAuthorName(void) const          { return OpenViBE::CString("Juan Grethe & Martin Grabina"); }
			virtual OpenViBE::CString getAuthorCompanyName(void) const   { return OpenViBE::CString("ITBA"); }
			virtual OpenViBE::CString getShortDescription(void) const    { return OpenViBE::CString("Visualizes the alphabet for P300 RSVP spellers"); }
			virtual OpenViBE::CString getDetailedDescription(void) const { return OpenViBE::CString(""); }
			virtual OpenViBE::CString getCategory(void) const            { return OpenViBE::CString("Visualization/Presentation"); }
			virtual OpenViBE::CString getVersion(void) const             { return OpenViBE::CString("1.0"); }
			virtual OpenViBE::CString getStockItemName(void) const       { return OpenViBE::CString("gtk-select-font"); }

			virtual OpenViBE::CIdentifier getCreatedClass(void) const    { return OVP_ClassId_BoxAlgorithm_P300RSVPSpellerVisualization; }
			virtual OpenViBE::Plugins::IPluginObject* create(void)       { return new OpenViBEPlugins::SimpleVisualization::CBoxAlgorithmP300RSVPSpellerVisualization; }

			virtual bool hasFunctionality(OpenViBE::CIdentifier functionalityIdentifier) const
			{
				return functionalityIdentifier == OVD_Functionality_Visualization;
			}

			virtual bool getBoxPrototype(
				OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const
			{
				rBoxAlgorithmPrototype.addInput ("Sequence stimulations",            OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput ("Target stimulations",              OV_TypeId_Stimulations);
				rBoxAlgorithmPrototype.addInput ("Char selection stimulations",       OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addOutput("Target / Non target flagging (deprecated)",     OV_TypeId_Stimulations);

				rBoxAlgorithmPrototype.addSetting("Interface filename",              OV_TypeId_Filename,    "${Path_Data}/plugins/simple-visualization/p300-speller.ui");
				rBoxAlgorithmPrototype.addSetting("Char stimulation base",            OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
				rBoxAlgorithmPrototype.addSetting("Target background color",         OV_TypeId_Color,       "10,40,10");
				rBoxAlgorithmPrototype.addSetting("Selected background color",       OV_TypeId_Color,       "70,20,20");

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_P300RSVPSpellerVisualizationDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_P300RSVPSpellerVisualization_H__
